<?php

namespace AppBundle\Tests\Service;

use AppBundle\Entity\Album;
use AppBundle\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class DefaultServiceTest
 */
class DefaultServiceTest extends KernelTestCase
{

    /**
     * setUp
     */
    public function setUp()
    {
        parent::setUp();

        static::bootKernel();
    }

    /** @test */
    public function testAlbums()
    {
        $albums = $this->albums();
        $this->assertCount(2, $albums);
        $album = $this->album();

        $this->assertCount(6, $album->getImages());
    }

    protected function albums()
    {
        $album = new Album();
        $album->setName('Test album 1');

        $album2 = new Album();
        $album2->setName('Test album 2');

        return [$album, $album2];
    }

    protected function album()
    {
        $album = new Album();
        $album->setName('Test album 1');
        $images = [];

        for ($i = 0; $i < 6; $i++) {
            $image = new Image();
            $image->setAlbum($album)
                ->setName('Image'.$i)
                ->setCreatedAt(new \DateTime());
            $images[] = $image;
            unset($image);
        }
        $album->addImages($images);

        return $album;
    }
}
