<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DefaultControllerTest
 */
class DefaultWebTest extends WebTestCase
{

    /** @test */
    public function testAlbums()
    {
        $client = self::createClient();

        $crawler = $client->request('GET', '/');
        $response = $client->getResponse();
        $this->assertEquals($response->getStatusCode(), 200);
        $this->assertGreaterThan(0, $crawler->filter('table > tbody > tr')->count());
        $this->assertGreaterThan(0, $crawler->filter('table > tbody > tr > .album-id')->count());
        $albumId = $crawler->filter('table > tbody > tr > .album-id')->first()->text();

        $client->request('GET', '/album/'.$albumId.'/page', [], [], [
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
        ]);
        $response = $client->getResponse();
        $this->assertEquals($response->getStatusCode(), 200);
        $this->assertTrue(
            $response->headers->contains('Content-Type', 'application/json'),
            $response->headers
        );
        $data = json_decode($response->getContent());

        $this->assertNotEmpty($data->pagination);
        $this->assertNotEmpty($data->pagination->items);
    }
}
