<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Album;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Class ImageRepository
 */
class ImageRepository extends EntityRepository
{
    /**
     * @param Album $album
     * @return Query
     */
    public function getPaginationQuery(Album $album)
    {
        return $this->createQueryBuilder('i')
            ->select('i')
            ->where('i.album = :album')
            ->setParameter('album', $album)
            ->getQuery();
    }
}
