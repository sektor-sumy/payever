<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Album;
use AppBundle\Entity\Image;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class Albums
 */
class Albums implements FixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 5; $i++) {
            $album = new Album();
            $album->setName('Test Album name #'.$i);
            $max = $i == 0 ? 5 : 25;
            for ($n = 0; $n < $max; $n++) {
                $image = file_get_contents(realpath('web/for_fixture/test.png'));
                file_put_contents('web/uploads/test_'.$i.'_'.$n.'.png', $image);
                $file = new File(realpath('web/uploads/test_'.$i.'_'.$n.'.png'));
                $image = new Image();
                $image->setAlbum($album);
                $image->setFile($file);
                $manager->persist($image);
            }
            $manager->persist($album);
        }
        $manager->flush();
    }
}
