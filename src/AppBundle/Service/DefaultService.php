<?php

namespace AppBundle\Service;

use AppBundle\Entity\Album;
use AppBundle\Serializer\PaginatorSerializer;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension;
use Knp\Component\Pager\Paginator;

/**
 * Class DefaultService
 */
class DefaultService
{
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var Paginator
     */
    private $paginator;

    /**
     * @var PaginatorSerializer
     */
    private $paginatorSerializer;
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var PaginationExtension
     */
    private $paginationExtension;

    /**
     * DefaultService constructor.
     * @param ObjectManager       $entityManager
     * @param Paginator           $paginator
     * @param PaginatorSerializer $paginatorSerializer
     *
     */
    public function __construct(
        ObjectManager $entityManager,
        Paginator $paginator,
        PaginatorSerializer $paginatorSerializer,
        \Twig_Environment $twig,
		PaginationExtension $paginationExtension
    )
    {
        $this->em = $entityManager;
        $this->paginator = $paginator;
        $this->paginatorSerializer = $paginatorSerializer;
        $this->twig = $twig;
        $this->paginationExtension = $paginationExtension;
    }

    /**
     * @param Album $album
     * @param int   $page
     * @return array
     */
    public function images(Album $album, $page = 1)
    {
        $query = $this->em->getRepository("AppBundle:Image")->getPaginationQuery($album);

        /** @var SlidingPagination $images */
        $images = $this->paginator->paginate($query, $page);
        $pagination = $this->paginatorSerializer->normalize($images);
        $pagination['paginationHtml'] = $this->paginationExtension->render($this->twig, $images);

        return $pagination;
    }
}
