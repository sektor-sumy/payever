<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File as HttpFile;

/**
 * @ORM\Table(name="image")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImageRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Image
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var Album
     * @ORM\ManyToOne(targetEntity="Album", inversedBy="images")
     * @ORM\JoinColumn(name="album_id", referencedColumnName="id")
     */
    private $album;
    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;
    /**
     * @ORM\Column(name="name", type="text", nullable=false)
     */
    private $name;
    /**
     * @ORM\Column(name="format", type="text", nullable=false)
     */
    private $format;
    /**
     * @ORM\Column(name="filename", type="text", nullable=false)
     * @JMS\Exclude
     */
    protected $filename;

    protected $file;

    protected $url;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        if (!$this->name) {
            $this->name = $this->getId();
        }

        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     * @return $this
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * @return string
     */
    public function getUploadRootDir()
    {
        return realpath(__DIR__.'/../../../web/uploads');
    }

    /**
     * @return string
     */
    public function getUploadDir()
    {
        return implode(DIRECTORY_SEPARATOR, [$this->getUploadRootDir(), $this->getUploadSubDir()]);
    }

    /**
     * @return null|string
     */
    public function getRelativePath()
    {
        $path = null;
        if ($this->getFilename()) {
            $path = implode(DIRECTORY_SEPARATOR, [
                $this->getUploadSubDir(),
                $this->getFilename(),
            ]);
        }

        return $path;
    }

    /**
     * @return string
     */
    public function getUploadSubDir()
    {
        return implode(DIRECTORY_SEPARATOR, [
            'album_image',
            substr($this->getFilename(), 0, 2),
            substr($this->getFilename(), 2, 2),
        ]);
    }

    /**
     * @return null|string
     */
    public function getAbsolutePath()
    {
        $path = null;
        if ($relativePath = $this->getRelativePath()) {
            $path = implode(DIRECTORY_SEPARATOR, [$this->getUploadRootDir(), $relativePath]);
        }

        return $path;
    }

    /**
     * @param HttpFile $file
     * @return $this
     */
    public function setFile(HttpFile $file)
    {
        $this->file = $file;
        if (!$this->format) {
            $this->format = $file->guessExtension();
        }
        if (!$this->filename) {
            $this->filename = sha1(uniqid(mt_rand(), true)).'.'.$this->format;
            $this->name = substr(md5(mt_rand()), 0, 8).'.'.$this->format;
        }

        return $this;
    }

    /**
     * @return HttpFile
     */
    public function getFile()
    {
        $path = $this->getAbsolutePath();
        if (!$this->file && file_exists($path)) {
            $this->file = new HttpFile($path);
        }

        return $this->file;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return mixed
     */
    public function setUrl($url)
    {
        return $this->url = $url;
    }

    /**
     * @param Album $album
     * @return $this
     */
    public function setAlbum(Album $album)
    {
        $this->album = $album;

        return $this;
    }

    /**
     * @return Album
     */
    public function getAlbum()
    {
        return $this->album;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function checkFileIsReadable()
    {
        $file = $this->getFile();
        if (!$file || !$file->isReadable()) {
            throw new \RuntimeException('File not exists or not readable');
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function moveFileToUploadDir()
    {
        $file = $this->getFile();
        if ($file->getPath() !== $this->getAbsolutePath()) {
            $file = $file->move($this->getUploadDir(), $this->getFilename());
            $this->setFile($file);
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeFile()
    {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }
}
