<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="album")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AlbumRepository")
 */
class Album
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var ArrayCollection|Image[]
     * @ORM\OneToMany(targetEntity="Image", mappedBy="album", cascade={"persist"})
     * @ORM\OrderBy({"createdAt" = "DESC", "id" = "DESC"})
     */
    private $images;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Album
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add image
     *
     * @param Image $image
     * @return Image
     */
    public function addImage(Image $image = null)
    {
        if (!is_null($image)) {
            $this->images[] = $image;
            $image->setAlbum($this);
        }

        return $this;
    }
    /**
     * Add images
     *
     * @param Image[]|ArrayCollection $images
     * @return Image
     */
    public function addImages($images)
    {
        foreach ($images as $image) {
            $this->addImage($image);
        }

        return $this;
    }
    /**
     * Remove image
     *
     * @param Image $image
     */
    public function removeImage(Image $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * @return Image[]|ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }
}
