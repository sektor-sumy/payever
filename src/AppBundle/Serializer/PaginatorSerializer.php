<?php

namespace AppBundle\Serializer;

use Knp\Component\Pager\Paginator;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class PaginatorSerializer
 */
class PaginatorSerializer implements NormalizerInterface
{
    /**
     * @param mixed  $data
     * @param string $format
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Paginator;
    }

    /**
     * @param mixed  $object
     * @param string $format
     * @param array  $context
     * @return mixed
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $callback = function ($dateTime) {
            return $dateTime instanceof \DateTime
                ? $dateTime->format('Y-m-d')
                : '';
        };
        $normalizer = new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter());
        $normalizer->setCallbacks(['createdAt' => $callback]);
        $normalizer->setIgnoredAttributes(['file', 'album']);
        $normalizeItem = function ($value) use ($normalizer) {
            return $normalizer->normalize($value);
        };

        return [
            'currentPageNumber' => (int) $object->getCurrentPageNumber(),
            'numItemsPerPage' => (int) $object->getItemNumberPerPage(),
            'totalCount' => (int) $object->getTotalItemCount(),
            'pageCount' => (int) $object->getPageCount(),
            'items' => array_map($normalizeItem, $object->getItems()),
        ];
    }
}