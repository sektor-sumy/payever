<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="default-album")
     * @Template()
     * @return array
     */
    public function indexAction()
    {
        $albums = $this->getDoctrine()->getRepository('AppBundle:Album')->findBy([], ['id' => 'ASC']);

        return [
            'albums' => $albums,
        ];
    }

    /**
     * @Route("/album/{id}/page/{page}", name="default-album-view", defaults={"page" = 1}, requirements={"page" = "\d+"})
     * @Template()
     * @param int     $id
     * @param int     $page
     * @param Request $request
     * @return array
     */
    public function albumAction($id, $page, Request $request)
    {
        $album = $this->getDoctrine()->getRepository('AppBundle:Album')->findOneBy(['id' => $id]);
        if (!$album) {
            $this->createNotFoundException('Album not found.');
        }
        $pagination = $this->container->get('service.default')->images($album, $page);
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse(['pagination' => $pagination]);
        }

        return [
            'pagination' => $pagination,
        ];
    }
}
