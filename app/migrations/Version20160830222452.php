<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20160830222452
 */
class Version20160830222452 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
            CREATE TABLE album
            (
                id INT NOT NULL AUTO_INCREMENT,
                name TEXT NOT NULL,
                UNIQUE (name(256)),
                PRIMARY KEY(id)
            );
        ");

        $this->addSql("
            CREATE TABLE image
            (
                id INT NOT NULL AUTO_INCREMENT,
                name TEXT DEFAULT NULL,
                album_id INT NOT NULL,
                format TEXT NOT NULL,
                filename TEXT NOT NULL,
                created_at TIMESTAMP(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY(id)
            );
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE image;");
        $this->addSql("DROP TABLE album;");
    }
}
