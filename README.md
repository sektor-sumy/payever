composer install

npm install

bower install

grunt (run grunt)

app/console doctrine::database:create

app/console doctrine:migrations:migrate

app/console doctrine:fixtures:load

phpunit -c app